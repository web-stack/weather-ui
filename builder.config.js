module.exports = {
  // directory in which bundle is created
  output: "dist",

  // dev server port
  port: 8081,

  // index.html template location,
  // leave undefined to use pre defined template
  template: "./public/index.html",

  assets: "./public",

  entries: {
    index: "src/index.tsx",
  },

  secure: true,

  storybook: {
    componentsDir: "./",
  },

  proxy: {},
};
