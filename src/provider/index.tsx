import { IHasChildren } from "@ismithi/ui-kit/interfaces";
import { ThemeProvider } from "@ismithi/ui-kit/styles";
import theme from "@styles/theme";
import React from "react";
import ServicesProvider from "./ServicesProvider";

function Providers(props: IHasChildren) {
  return (
    <ThemeProvider theme={theme}>
      <ServicesProvider>{props.children}</ServicesProvider>
    </ThemeProvider>
  );
}

export default Providers;
