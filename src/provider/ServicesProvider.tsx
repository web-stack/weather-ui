import { IHasChildren } from "@ismithi/ui-kit/interfaces";
import GeolocationService, {
  IGeolocationService,
} from "@service/geolocation/GeolocationService";
import OpenWeatherService, {
  IOpenWeatherService,
} from "@service/openWeather/OpenWeatherService";
import { IWeatherService } from "@service/weather/WeatherService";
import weatherServiceFactory from "@service/weather/weatherService.factory";
import React from "react";

type Services = {
  weatherService: IWeatherService;
  openWeatherService: IOpenWeatherService;
  geolocationService: IGeolocationService;
};

const openWeatherService = new OpenWeatherService();
const geolocationService = new GeolocationService();
const weatherService = weatherServiceFactory({
  openWeatherService,
  geolocationService,
});

const services: Services = {
  weatherService,
  openWeatherService,
  geolocationService,
};

export const ServicesProviderContext = React.createContext<Services>(services);

function ServicesProvider(props: IHasChildren) {
  return (
    <ServicesProviderContext.Provider value={services}>
      {props.children}
    </ServicesProviderContext.Provider>
  );
}

export default ServicesProvider;
