import Container from "@components/container/Container";
import HomePage from "@page/HomePage";
import Providers from "@provider/index";
import React from "react";
import hot from "@ismithi/ui-builder/hot";
import "./App.css";
import "mobx-react-lite/batchingForReactDom";

function App() {
  return (
    <Providers>
      <Container>
        <HomePage />
      </Container>
    </Providers>
  );
}

export default hot(App);
