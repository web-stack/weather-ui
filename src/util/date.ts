const daysDictionary = [
  "monday",
  "tuesday",
  "wednesday",
  "thursday",
  "friday",
  "saturday",
  "sunday",
];

function getDayLabel(day: number) {
  return daysDictionary[day];
}

export default {
  getDayLabel,
};
