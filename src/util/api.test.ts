import ApiConfig from "@config/ApiConfig";
import api from "./api";

describe("api test", function () {
  it("should get params from given url", function () {
    const url = "/some/api/call?name=John&age=20";
    const params = api.getUrlParams(url);

    expect(params.name).toBe("John");
    expect(parseInt(params.age)).toBe(20);
  });

  it("should extract only resource path from url", function () {
    const url = "/some/api/call?name=John&age=20";
    const resource = api.extractResourceFromUrl(url);

    expect(resource).toBe("/some/api/call");
  });

  it("should convert object to url params string", function () {
    const params = {
      name: "John",
      age: 20,
    };

    const paramsString = api.recordToUrlParams(params);
    expect(paramsString).toBe("name=John&age=20");
  });

  it("should create a valid api url", function () {
    const apiUrl = api.makeApiUrl("/onecall?lon=43.23&lat=24.87");

    expect(apiUrl).toBe(
      `${ApiConfig.apiUrl}/onecall?lon=43.23&lat=24.87&appId=${ApiConfig.appId}`
    );
  });
});
