import ApiConfig from "@config/ApiConfig";

async function get(path: string) {
  const url = makeApiUrl(path);
  return await fetch(url)
    .then(validateResponse)
    .then((res) => res.json());
}

function validateResponse(response: Response) {
  if (!response.ok) {
    throw new Error(response.statusText);
  }

  return response;
}

function makeApiUrl(path: string) {
  const params = { ...getUrlParams(path), appId: ApiConfig.appId };
  const paramsString = recordToUrlParams(params);
  const resource = extractResourceFromUrl(path);

  return `${ApiConfig.apiUrl}${resource}?${paramsString}`;
}

function getUrlParams(url: string): Record<string, any> {
  return url
    .split("?")[1]
    ?.split("&")
    .reduce((acc, valuePair) => {
      const [key, value] = valuePair.split("=");

      return { ...acc, [key]: value };
    }, {});
}

function extractResourceFromUrl(url: string) {
  const [resource] = url.split("?");

  return resource;
}

function recordToUrlParams(record: Record<string, any>): string {
  return Object.entries(record)
    .map(([key, value]) => `${key}=${value}`)
    .join("&");
}

export default {
  get,
  makeApiUrl,
  getUrlParams,
  extractResourceFromUrl,
  recordToUrlParams,
};
