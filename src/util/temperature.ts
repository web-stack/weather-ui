function kelvinToCelsius(temperature: number) {
  return Math.round((temperature - 273) * 10) / 10;
}

export default {
  kelvinToCelsius,
};
