import HomePageLayout from "@page/HomePage.layout";
import HomePageContext from "@page/HomePageContext";
import { ServicesProviderContext } from "@provider/ServicesProvider";
import { observer } from "mobx-react-lite";
import React, { useContext, useEffect } from "react";

function HomePage() {
  const { openWeatherService, weatherService } = useContext(
    ServicesProviderContext
  );

  useEffect(() => {
    try {
      weatherService.loadData();
    } catch (e) {
      console.log(e);
    }
  }, []);

  return (
    <HomePageContext.Provider value={{ loadAll: weatherService.loadData }}>
      <HomePageLayout
        weather={openWeatherService.weather}
        forecast={openWeatherService.forecast}
      />
    </HomePageContext.Provider>
  );
}

export default observer(HomePage);
