import LocationInput from "@components/locationInput/LocationInput";
import SlideAndFade from "@components/slideAndFade/SlideAndFade";
import { Box } from "@ismithi/ui-kit/components";
import { ServicesProviderContext } from "@provider/ServicesProvider";
import { observer } from "mobx-react-lite";
import React, { useContext } from "react";

function CityInput() {
  const { openWeatherService } = useContext(ServicesProviderContext);
  const handleTypingStop = async (value: string) => {
    await Promise.all([
      openWeatherService.loadWeather({ q: value }),
      openWeatherService.loadForecast({ q: value }),
    ]);
  };

  return (
    <Box pb={2}>
      <SlideAndFade>
        <LocationInput onTypingStop={handleTypingStop} />
      </SlideAndFade>
    </Box>
  );
}

export default observer(CityInput);
