import ForecastCard from "@components/forecastCard/ForecastCard";
import SlideAndFade from "@components/slideAndFade/SlideAndFade";
import { Box, Row } from "@ismithi/ui-kit/components";
import WeatherModel from "@model/WeatherModel";
import date from "@util/date";
import temperature from "@util/temperature";
import React from "react";
import styles from "./ForecastWeather.styles.scss";

interface IProps {
  days: WeatherModel[];
}

function ForecastWeather(props: IProps) {
  return (
    <>
      <Box px={2} pt={1}>
        <SlideAndFade>
          <h3>Forecast</h3>
        </SlideAndFade>
      </Box>
      <SlideAndFade>
        <Box className={styles.slider}>
          <Row wrap={false}>
            {props.days.map((day) => (
              <Box px={1}>
                <ForecastCard
                  icon={day.condition}
                  temperature={temperature.kelvinToCelsius(day.temperature)}
                  subtitle={date.getDayLabel(day.timestamp.getDay())}
                />
              </Box>
            ))}
          </Row>
        </Box>
      </SlideAndFade>
    </>
  );
}

export default ForecastWeather;
