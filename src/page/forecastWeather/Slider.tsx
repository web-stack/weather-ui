import { IHasChildren } from "@ismithi/ui-kit/interfaces";
import React, { useRef } from "react";
import { animated, useSpring } from "react-spring";
import { useGesture } from "react-use-gesture";

function Slider(props: IHasChildren) {
  const div = useRef<HTMLDivElement | null>();
  const movement = useRef(0);
  const delta = useRef(0);
  const [{ x }, set] = useSpring(() => ({
    x: 0,
    config: {
      mass: 0.2,
    },
  }));
  const bind = useGesture({
    onDrag: ({ movement: [dx] }) => {
      set({ x: dx + delta.current });
      movement.current = dx;
    },
    onDragEnd: () => {
      delta.current += movement.current;
      if (delta.current > 0) {
        set({ x: 0 });
        delta.current = 0;
      }

      // if (div.current) {
      //   const end = -(div.current.offsetWidth - window.innerWidth + 16);
      //   if (delta.current < end) {
      //     set({ x: end });
      //     delta.current = end;
      //   }
      // }
    },
  });

  return (
    <animated.div
      {...bind()}
      style={{
        overflow: "hidden",
        transform: x.interpolate((x) => `translateX(${x}px)`),
      }}
    >
      <div ref={(r) => (div.current = r)}>{props.children}</div>
    </animated.div>
  );
}

export default Slider;
