import React from "react";

interface IHomePageContext {
  loadAll(city?: string): Promise<any>;
}

const HomePageContext = React.createContext<IHomePageContext | null>(null);

export default HomePageContext;
