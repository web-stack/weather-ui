import { Box } from "@ismithi/ui-kit/components";
import React from "react";

function CoordsPending() {
  return (
    <Box p={1}>
      <h1>Pending coordinates...</h1>
    </Box>
  );
}

export default CoordsPending;
