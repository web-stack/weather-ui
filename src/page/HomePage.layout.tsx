import SlideAndFade from "@components/slideAndFade/SlideAndFade";
import { Box } from "@ismithi/ui-kit/components";
import WeatherModel from "@model/WeatherModel";
import CityInput from "@page/cityInput/CityInput";
import ForecastWeather from "@page/forecastWeather/ForecastWeather";
import TodayWeather from "@page/todaysWeather/TodayWeather";
import React from "react";

interface IProps {
  weather: WeatherModel | null;
  forecast: WeatherModel[] | null;
}

function HomePageLayout(props: IProps) {
  return (
    <>
      <Box p={2}>
        <CityInput />
        {props.weather && <TodayWeather {...props.weather} />}
      </Box>
      {props.forecast && <ForecastWeather days={props.forecast} />}
    </>
  );
}

export default HomePageLayout;
