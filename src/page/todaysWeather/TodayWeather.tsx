import SlideAndFade from "@components/slideAndFade/SlideAndFade";
import WeatherIcon from "@components/weatherIcon/WeatherIcon";
import { Box, Row } from "@ismithi/ui-kit/components";
import WeatherModel from "@model/WeatherModel";
import temperature from "@util/temperature";
import React from "react";
import styles from "./TodaysWeather.styles.scss";

interface IProps extends WeatherModel {}

function TodayWeather(props: IProps) {
  return (
    <>
      <SlideAndFade>
        <h3>Today</h3>
      </SlideAndFade>
      <SlideAndFade delay={100}>
        <Box>
          <Row spacing={1} alignItems="center">
            <WeatherIcon name={props.condition} size="large" />
            <Box>
              <h4 className={styles.description}>
                {temperature.kelvinToCelsius(props.temperature)}&deg;C
              </h4>
              <h4 className={styles.description}>{props.description}</h4>
            </Box>
          </Row>
        </Box>
      </SlideAndFade>
    </>
  );
}

export default TodayWeather;
