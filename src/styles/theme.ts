import { ITheme, themes } from "@ismithi/ui-kit/styles";

const theme: ITheme = {
  ...themes.defaultTheme,
  typography: {
    ...themes.defaultTheme.typography,
    fontFamilies: {
      ...themes.defaultTheme.typography.fontFamilies,
      primary: "Nunito, Roboto, sans-serif",
    },
  },
  shapes: {
    borderRadius: "12px",
    spacing: 8,
  },
  palette: {
    light: {
      background: {
        light: "#fff",
        main: "#fff",
        dark: "#F9F8F0",
      },
      text: {
        light: "#3B3B3B",
        main: "#000",
        dark: "#000",
      },
      gray: {
        light: "#797979",
        main: "#5A5A5A",
        dark: "#3C3C3C",
      },
      primary: {
        light: "#797979",
        main: "#5A5A5A",
        dark: "#3C3C3C",
      },
      secondary: {
        light: "#797979",
        main: "#5A5A5A",
        dark: "#3C3C3C",
      },
    },
  },
};

export default theme;
