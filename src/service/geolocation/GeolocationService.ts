import { action, observable } from "mobx";

export interface IGeolocationService {
  readonly city: string | null;
  readonly coords: Coordinates | null;
  readonly noGeolocationPermissions: boolean;

  setCity(city: string): void;
  setCoords(coords: Coordinates): void;
  loadCoords(): Promise<Coordinates>;
}

class GeolocationService implements IGeolocationService {
  @observable
  coords: Coordinates | null = null;

  @observable
  city: string | null = null;

  @observable
  noGeolocationPermissions = false;

  async loadCoords(): Promise<Coordinates> {
    if (!navigator.geolocation) {
      throw new Error("geolocation is not supported");
    }

    return new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          resolve(position.coords);
        },
        (error) => {
          reject(error.message);
          this.noGeolocationPermissions = true;
        }
      );
    });
  }

  @action
  setCity(city: string) {
    this.city = city;
  }

  @action
  setCoords(coords: Coordinates) {
    this.coords = coords;
  }
}

export default GeolocationService;
