import { IGeolocationService } from "@service/geolocation/GeolocationService";
import { IOpenWeatherService } from "@service/openWeather/OpenWeatherService";
import ForecastResponse from "@service/openWeather/response/ForecastResponse";
import WeatherResponse from "@service/openWeather/response/WeatherResponse";

export interface IWeatherService {
  loadData(city?: string): Promise<[WeatherResponse, ForecastResponse]>;
}

class WeatherService implements IWeatherService {
  private _openWeatherService: IOpenWeatherService;
  private _geolocationService: IGeolocationService;

  constructor(
    openWeatherService: IOpenWeatherService,
    geolocationService: IGeolocationService
  ) {
    this._openWeatherService = openWeatherService;
    this._geolocationService = geolocationService;
  }

  async loadData(city?: string): Promise<[WeatherResponse, ForecastResponse]> {
    const data = city ? { q: city } : await this.getCoords();

    return await Promise.all([
      this._openWeatherService.loadWeather(data),
      this._openWeatherService.loadForecast(data),
    ]);
  }

  private async getCoords() {
    const coords = await this._geolocationService.loadCoords();
    this._geolocationService.setCoords(coords);
    return {
      lat: coords.latitude,
      lon: coords.longitude,
    };
  }
}

export default WeatherService;
