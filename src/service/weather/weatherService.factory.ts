import { IGeolocationService } from "@service/geolocation/GeolocationService";
import { IOpenWeatherService } from "@service/openWeather/OpenWeatherService";
import WeatherService from "@service/weather/WeatherService";

interface IWeatherServiceConstructor {
  openWeatherService: IOpenWeatherService;
  geolocationService: IGeolocationService;
}

function weatherServiceFactory(dependencies: IWeatherServiceConstructor) {
  return new WeatherService(
    dependencies.openWeatherService,
    dependencies.geolocationService
  );
}

export default weatherServiceFactory;
