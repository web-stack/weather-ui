import WeatherModel from "@model/WeatherModel";
import OpenForecastRequest from "@service/openWeather/request/OpenForecastRequest";
import OpenWeatherRequest from "@service/openWeather/request/OpenWeatherRequest";
import ForecastResponse, {
  ForecastListItem,
} from "@service/openWeather/response/ForecastResponse";
import WeatherResponse from "@service/openWeather/response/WeatherResponse";
import api from "@util/api";
import { action, observable, runInAction } from "mobx";

export interface IOpenWeatherService {
  readonly weather: WeatherModel | null;
  readonly forecast: WeatherModel[] | null;

  loadWeather(request: OpenWeatherRequest): Promise<WeatherResponse>;

  loadForecast(request: OpenForecastRequest): Promise<ForecastResponse>;
}

class OpenWeatherService implements IOpenWeatherService {
  @observable
  weather: WeatherModel | null = null;

  @observable
  forecast: WeatherModel[] | null = null;

  async loadForecast(request: OpenForecastRequest): Promise<ForecastResponse> {
    const params = api.recordToUrlParams(request);
    const result = (await api.get(`/forecast?${params}`)) as ForecastResponse;

    this.saveForecast(result);

    return result;
  }

  async loadWeather(request: OpenWeatherRequest): Promise<WeatherResponse> {
    const params = api.recordToUrlParams(request);
    const result = (await api.get(`/weather?${params}`)) as WeatherResponse;

    runInAction("save weather", () => {
      this.weather = {
        timestamp: new Date(),
        description: result.weather[0].description,
        condition: result.weather[0].main,
        temperature: result.main.temp,
      };
    });

    return result;
  }

  @action
  private saveForecast(forecast: ForecastResponse) {
    this.forecast = forecast.list
      /**
       * Data is every 3 hours forecast - we need to get only by days.
       * With this function we add forecast to list only if day is not present in list already.
       */
      .reduce((acc: ForecastListItem[], listItem) => {
        const hasItem = acc.some(
          (item) =>
            new Date(item.dt_txt).getDate() ===
            new Date(listItem.dt_txt).getDate()
        );
        if (hasItem) {
          return acc;
        }

        return [...acc, listItem];
      }, [])
      .map((listItem) => {
        return {
          temperature: listItem.main.temp,
          condition: listItem.weather[0].main,
          description: listItem.weather[0].description,
          timestamp: new Date(listItem.dt_txt),
        };
      });
  }
}

export default OpenWeatherService;
