import WeatherData from "@service/openWeather/response/WeatherData";

type MainTemp = {
  temp: number;
  feels_like: number;
  temp_min: number;
  temp_max: number;
  pressure: number;
  humidity: number;
};

export type ForecastListItem = {
  dt_txt: string;
  dt: number;
  main: MainTemp;
  weather: WeatherData[];
};

export default interface ForecastResponse {
  list: ForecastListItem[];
}
