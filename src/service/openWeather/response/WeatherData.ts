import WeatherKey from "@model/WeatherKey";

interface WeatherData {
  id: number;
  main: WeatherKey;
  description: string;
  icon: string;
}

export default WeatherData;
