import WeatherData from "@service/openWeather/response/WeatherData";

export default interface WeatherResponse {
  coord: {
    lat: number;
    lon: number;
  };
  weather: WeatherData[];
  main: {
    temp: number;
    feels_like: number;
    temp_min: number;
    temp_max: number;
    pressure: number;
    humidity: number;
  };
}
