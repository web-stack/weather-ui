interface OpenWeatherRequest {
  lat?: number;
  lon?: number;
  q?: string;
}

export default OpenWeatherRequest;
