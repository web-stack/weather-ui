interface OpenForecastRequest {
  lat?: number;
  lon?: number;
  q?: string;
}

export default OpenForecastRequest;
