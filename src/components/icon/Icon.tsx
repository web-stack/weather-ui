import React from "react";

const loadIcon = (name: string) => require(`./images/${name}.svg`).default;

export interface IIconProps extends React.SVGProps<SVGSVGElement> {
  name: string;
}

function Icon({ name, ...props }: IIconProps) {
  const LoadedIcon = loadIcon(name);

  return <LoadedIcon {...props} />;
}

export default Icon;
