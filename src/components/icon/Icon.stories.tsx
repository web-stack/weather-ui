import Icon from "@components/icon/Icon";
import React from "react";

export default {
  title: "Icon",
};

export const Icons = () => (
  <>
    <Icon name="sunny" />
    <Icon name="sun_clouds" />
    <Icon name="clouds" />
    <Icon name="rain" />
    <Icon name="thunder" />
  </>
);
