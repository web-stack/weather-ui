import LocationInput from "@components/locationInput/LocationInput";
import { ThemeProvider } from "@ismithi/ui-kit/styles";
import theme from "@styles/theme";
import React from "react";

export default {
  title: "Location input",
};

export const DefaultInput = () => (
  <ThemeProvider theme={theme}>
    <LocationInput onTypingStop={(value) => console.log(value)} />
  </ThemeProvider>
);
