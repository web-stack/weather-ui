import { Input } from "@ismithi/ui-kit/components";
import React, { useRef } from "react";
import styles from "./LocationInput.styles.scss";

interface IProps {
  onTypingStop?: (value: string) => void;
}

function LocationInput(props: IProps) {
  const timer = useRef<any>();
  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (timer.current) {
      clearTimeout(timer.current);
    }

    const value = e.target.value;
    timer.current = setTimeout(() => {
      if (props.onTypingStop) {
        props.onTypingStop(value);
      }
    }, 1000);
  };

  return (
    <Input
      className={styles.root}
      name="location"
      label="City name"
      onChange={handleChange}
    />
  );
}

export default LocationInput;
