import ForecastCard from "@components/forecastCard/ForecastCard";
import { Row } from "@ismithi/ui-kit/components";
import { ThemeProvider } from "@ismithi/ui-kit/styles";
import theme from "@styles/theme";
import React from "react";

export default {
  title: "Forecast card",
};

export const DefaultCard = () => (
  <ThemeProvider theme={theme}>
    <Row spacing={1}>
      <ForecastCard icon="Clear" temperature={26} subtitle="monday" />
      <ForecastCard icon="Rain" temperature={14} subtitle="tuesday" />
      <ForecastCard icon="Clouds" temperature={18} subtitle="wednesday" />
    </Row>
  </ThemeProvider>
);
