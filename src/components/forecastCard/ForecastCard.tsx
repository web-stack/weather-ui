import Icon from "@components/icon/Icon";
import WeatherIcon from "@components/weatherIcon/WeatherIcon";
import { Box, Card, Column } from "@ismithi/ui-kit/components";
import WeatherKey from "@model/WeatherKey";
import React, { ReactNode } from "react";
import styles from "./ForecastCard.styles.scss";

interface IProps {
  icon: WeatherKey;
  temperature: string | number;
  subtitle: string | ReactNode;
}

function ForecastCard(props: IProps) {
  return (
    <div className={styles.card}>
      <Column alignItems="center">
        <WeatherIcon
          width={48}
          height={48}
          viewBox="0 0 26 26"
          name={props.icon}
        />
        <Box py={1}>
          {typeof props.temperature === "string" ? (
            <h3 className={styles.temperature}>{props.temperature}</h3>
          ) : (
            <h3 className={styles.temperature}>{props.temperature} °C</h3>
          )}
        </Box>
        {typeof props.subtitle === "string" ? (
          <p className={styles.subtitle}>{props.subtitle}</p>
        ) : (
          <>{props.subtitle}</>
        )}
      </Column>
    </div>
  );
}

export default ForecastCard;
