import WeatherIcon from "@components/weatherIcon/WeatherIcon";
import { Column, Row } from "@ismithi/ui-kit/components";
import { ThemeProvider } from "@ismithi/ui-kit/styles";
import theme from "@styles/theme";
import React from "react";

export default {
  title: "Weather icon",
};

export const Icons = () => (
  <ThemeProvider theme={theme}>
    <Column spacing={1}>
      <Row spacing={1} alignItems="center">
        <WeatherIcon name="Clear" />
        <WeatherIcon name="Clouds" />
        <WeatherIcon name="Rain" />
        <WeatherIcon name="Thunderstorm" />
      </Row>
      <Row spacing={1} alignItems="center">
        <WeatherIcon name="Clear" size="regular" />
        <WeatherIcon name="Clouds" size="regular" />
        <WeatherIcon name="Rain" size="regular" />
        <WeatherIcon name="Thunderstorm" size="regular" />
      </Row>
      <Row spacing={1} alignItems="center">
        <WeatherIcon name="Clear" size="large" />
        <WeatherIcon name="Clouds" size="large" />
        <WeatherIcon name="Rain" size="large" />
        <WeatherIcon name="Thunderstorm" size="large" />
      </Row>
    </Column>
  </ThemeProvider>
);
