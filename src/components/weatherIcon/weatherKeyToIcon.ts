import WeatherKey from "@model/WeatherKey";

type Dictionary = {
  [key in WeatherKey]: string;
};

const dictionary: Dictionary = {
  Clear: "sunny",
  Clouds: "clouds",
  Drizzle: "rain",
  Dust: "clouds",
  Haze: "clouds",
  Mist: "rain",
  Rain: "rain",
  Smoke: "clouds",
  Snow: "rain",
  Thunderstorm: "thunder",
};

function weatherKeyToIcon(weatherKey: WeatherKey) {
  return dictionary[weatherKey];
}

export default weatherKeyToIcon;
