export type Size = "small" | "regular" | "large";

type SizeDictionary = {
  [key in Size]: {
    height: number;
    width: number;
    viewBox: string;
  };
};

const sizeDictionary: SizeDictionary = {
  small: {
    height: 24,
    width: 24,
    viewBox: "0 0 24 24",
  },
  regular: {
    height: 32,
    width: 32,
    viewBox: "0 0 24 24",
  },
  large: {
    height: 72,
    width: 72,
    viewBox: "0 0 24 24",
  },
};

function sizeKeyToSvgProps(size: Size) {
  return sizeDictionary[size];
}

export default sizeKeyToSvgProps;
