import Icon, { IIconProps } from "@components/icon/Icon";
import sizeKeyToSvgProps, {
  Size,
} from "@components/weatherIcon/sizeKeyToSvgProps";
import weatherKeyToIcon from "@components/weatherIcon/weatherKeyToIcon";
import WeatherKey from "@model/WeatherKey";
import React from "react";

interface IProps extends IIconProps {
  name: WeatherKey;
  size?: Size;
}

function WeatherIcon({ name, size = "small", ...props }: IProps) {
  return (
    <Icon
      name={weatherKeyToIcon(name)}
      {...sizeKeyToSvgProps(size)}
      {...props}
    />
  );
}

export default WeatherIcon;
