import { IHasChildren } from "@ismithi/ui-kit/interfaces";
import React from "react";
import styles from "./SlideAndFade.styles.scss";

interface IProps extends IHasChildren {
  delay?: number;
}

function SlideAndFade({ delay = 0, children }: IProps) {
  return (
    <div className={styles.slide} style={{ animationDelay: `${delay}ms` }}>
      <div className={styles.fade} style={{ animationDelay: `${delay}ms` }}>
        {children}
      </div>
    </div>
  );
}

export default SlideAndFade;
