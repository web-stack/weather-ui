import temperatureToBackgroundColorClass from "@components/container/temperatureToBackgroundColor";
import { IHasChildren } from "@ismithi/ui-kit/interfaces";
import { ServicesProviderContext } from "@provider/ServicesProvider";
import temperature from "@util/temperature";
import { observer } from "mobx-react-lite";
import React, { useContext } from "react";
import styles from "./Container.styles.scss";
import clsx from "clsx";

function Container(props: IHasChildren) {
  const { openWeatherService } = useContext(ServicesProviderContext);
  const temp = openWeatherService.weather?.temperature;
  const backgroundClass =
    typeof temp !== "undefined" &&
    temperatureToBackgroundColorClass(temperature.kelvinToCelsius(temp));

  return (
    <div className={clsx(styles.root, !!backgroundClass && backgroundClass)}>
      <div className={styles.container}>{props.children}</div>
    </div>
  );
}

export default observer(Container);
