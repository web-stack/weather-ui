import styles from "./Container.styles.scss";

function temperatureToBackgroundColorClass(temp: number) {
  if (temp > 30) {
    return styles.hotColor;
  }

  if (temp > 20) {
    return styles.warmColor;
  }

  if (temp > 12) {
    return styles.normalColor;
  }

  if (temp > 0) {
    return styles.coldColor;
  }

  return styles.freezingColor;
}

export default temperatureToBackgroundColorClass;
