import WeatherKey from "@model/WeatherKey";

export default interface WeatherModel {
  temperature: number;
  condition: WeatherKey;
  description: string;
  timestamp: Date;
}
