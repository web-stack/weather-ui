type WeatherKey = "Clear" | "Rain" | "Thunderstorm" | "Drizzle" | "Snow" | "Mist" | "Smoke" | "Haze" | "Dust" | "Clouds"

export default WeatherKey